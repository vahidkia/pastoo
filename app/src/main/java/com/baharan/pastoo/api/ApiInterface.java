package com.baharan.pastoo.api;

import com.baharan.pastoo.models.LoginRequest;
import com.baharan.pastoo.models.ProductResponse;
import com.baharan.pastoo.models.RegisterRequest;
import com.baharan.pastoo.models.RegisterResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ApiInterface {

    @POST("register")
    public Call<RegisterResponse> register(@Body RegisterRequest registerRequest);


    @POST("login")
    public Call<RegisterResponse> login(@Body LoginRequest loginRequest);

    @GET("getProducts")
    public Call<ProductResponse> getProducts();

}
