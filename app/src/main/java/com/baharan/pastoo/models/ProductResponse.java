package com.baharan.pastoo.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ProductResponse {

    String message;
    int status;

    @SerializedName( "data" )
    ArrayList<Product> products;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ArrayList<Product> getProducts() {
        return products;
    }

    public void setProducts(ArrayList<Product> products) {
        this.products = products;
    }
}
