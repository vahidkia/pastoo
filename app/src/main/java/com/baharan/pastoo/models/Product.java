package com.baharan.pastoo.models;

import android.os.Parcel;
import android.os.Parcelable;

public class Product implements Parcelable {

    int product_id;
    String name;
    String pic;
    int price;
    String description;
    Double lat;
    Double lang;

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLang() {
        return lang;
    }

    public void setLang(Double lang) {
        this.lang = lang;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Product() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.product_id);
        dest.writeString(this.name);
        dest.writeString(this.pic);
        dest.writeInt(this.price);
        dest.writeString(this.description);
        dest.writeValue(this.lat);
        dest.writeValue(this.lang);
    }

    protected Product(Parcel in) {
        this.product_id = in.readInt();
        this.name = in.readString();
        this.pic = in.readString();
        this.price = in.readInt();
        this.description = in.readString();
        this.lat = (Double) in.readValue(Double.class.getClassLoader());
        this.lang = (Double) in.readValue(Double.class.getClassLoader());
    }

    public static final Creator<Product> CREATOR = new Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel source) {
            return new Product(source);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };
}
