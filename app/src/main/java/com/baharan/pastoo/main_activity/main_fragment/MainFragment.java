package com.baharan.pastoo.main_activity.main_fragment;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.baharan.pastoo.R;
import com.baharan.pastoo.api.ApiInterface;
import com.baharan.pastoo.api.MyRetrofit;
import com.baharan.pastoo.models.Product;
import com.baharan.pastoo.models.ProductResponse;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class MainFragment extends Fragment implements AdapterProduct.MahsulListener {

    ApiInterface apiInterface;

    ProductResponse productResponse;

    ArrayList<Product>products=new ArrayList<>();
    AdapterProduct adapter;

    RecyclerView rv_product;

    ProgressBar progressBar;

    public MainFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        apiInterface= MyRetrofit.getRetrofit().create( ApiInterface.class );

        adapter=new AdapterProduct(requireContext(),products,this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate( R.layout.fragment_main, container, false );
        rv_product=view.findViewById(R.id.rv_product);

        progressBar=view.findViewById(R.id.prgress_product);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated( savedInstanceState );

        rv_product.setAdapter(adapter);


        apiInterface=MyRetrofit.getRetrofit().create(ApiInterface.class);

        Call<ProductResponse> call=apiInterface.getProducts();
        call.enqueue(new Callback<ProductResponse>() {
            @Override
            public void onResponse(Call<ProductResponse> call, Response<ProductResponse> response) {

                if (response.isSuccessful()){
                    productResponse=response.body();
                    products.addAll(productResponse.getProducts());
                    adapter.notifyDataSetChanged();
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(getActivity(),productResponse.getProducts().get(0).getName(),Toast.LENGTH_SHORT).show();

                }else {

                }

            }

            @Override
            public void onFailure(Call<ProductResponse> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Log.e("tts",t.getMessage());
                Toast.makeText(getActivity(), "عدم دسترسی به اینترنت", Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    public void mahsulCliked(Integer position) {

        Toast.makeText(requireContext(),products.get(position).getName() , Toast.LENGTH_SHORT).show();

    }
}
