package com.baharan.pastoo.main_activity.profile_fragment;


import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.baharan.pastoo.R;
import com.baharan.pastoo.api.ApiInterface;
import com.baharan.pastoo.api.MyRetrofit;
import com.baharan.pastoo.models.MyError;
import com.baharan.pastoo.models.RegisterRequest;
import com.baharan.pastoo.models.RegisterResponse;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegisterFragment extends Fragment {

    EditText et_email,et_name,et_password,et_repassword;

    Button btn_register;


    public RegisterFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate( R.layout.fragment_register, container, false );

        et_email=view.findViewById( R.id.et_email_register );
        et_name=view.findViewById( R.id.et_name_register );
        et_password=view.findViewById( R.id.et_password_register );
        et_repassword=view.findViewById( R.id.et_repassword_register );
        btn_register=view.findViewById( R.id.btn_register );

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated( savedInstanceState );
        btn_register.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name=et_name.getText().toString();
                String email=et_email.getText().toString();
                String password=et_password.getText().toString();
                String repassword=et_repassword.getText().toString();

                if (email.isEmpty()){
                    Toast.makeText( requireContext(), "لطفا ایمیل خود را وارد کنید", Toast.LENGTH_SHORT ).show();
                }else if (name.isEmpty()){
                    Toast.makeText( requireContext(), "لطفا نام خود را وارد کنید", Toast.LENGTH_SHORT ).show();
                }else if (password.length()<6){
                    Toast.makeText( requireContext(), " رمز عبور نباید کمتر از 6 کاراکتر باشد", Toast.LENGTH_SHORT ).show();
                }else if (!password.equals( repassword )){
                    Toast.makeText( requireContext(), "رمز عبور و تکرار آن یکسان نمی باشد", Toast.LENGTH_SHORT ).show();
                }else {

                    RegisterRequest registerRequest=new RegisterRequest( name,email,password );
                    ApiInterface apiInterface= MyRetrofit.getRetrofit().create( ApiInterface.class );


                    Call<RegisterResponse> registerResponseCall=apiInterface.register( registerRequest );
                    registerResponseCall.enqueue( new Callback<RegisterResponse>() {
                        @Override
                        public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                            if (response.isSuccessful()){
                                RegisterResponse registerResponse=response.body();
                                Toast.makeText( requireContext(), "ثبت نام با موفقیت انجام شد", Toast.LENGTH_SHORT ).show();


                            }else {
                                try {
                                    /*Log.e( "myerror",response.errorBody().string() );

                                    MyError myError=new Gson().fromJson( response.errorBody().string(),MyError.class );
                                    Toast.makeText( requireContext(), myError.getMessage(), Toast.LENGTH_SHORT ).show();*/

                                    JSONObject jsonObject=new JSONObject( response.errorBody().string() );
                                    Toast.makeText( getContext(), jsonObject.getString( "message" ), Toast.LENGTH_SHORT ).show();



                                } catch (IOException e) {
                                    e.printStackTrace();
                                }catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                        }

                        @Override
                        public void onFailure(Call<RegisterResponse> call, Throwable t) {
                            Toast.makeText( getContext(), "عدم دسترسی به اینترنت", Toast.LENGTH_SHORT ).show();


                        }
                    } );
                }
            }
        } );
    }
}
