package com.baharan.pastoo.main_activity.main_fragment;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.baharan.pastoo.R;
import com.baharan.pastoo.models.Product;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class AdapterProduct extends RecyclerView.Adapter<AdapterProduct.Holder>{
    Context context;
    ArrayList<Product>products ;

    String pic_base_address="https://poushka.com/shop/public/images/product/";

    MahsulListener mahsulListener;

    public AdapterProduct(Context context, ArrayList<Product>products,MahsulListener mahsulListener){
        this.context=context;
        this.products=products;
        this.mahsulListener=mahsulListener;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.custom_product,parent,false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final Holder holder, int position) {

        holder.tv_name.setText(products.get(position).getName());
        holder.tv_price.setText(products.get(position).getPrice() +" ");
        Glide.with(context).load(pic_base_address+products.get(position).getPic())
                .error(R.drawable.ic_picture_error)
                .into(holder.iv_pic);
         holder.itemView.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 mahsulListener.mahsulCliked(holder.getAdapterPosition());
             }
         });



    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    class  Holder extends RecyclerView.ViewHolder{

        ImageView iv_pic;
        TextView tv_name,tv_price;

        public Holder(@NonNull View itemView) {
            super(itemView);
            iv_pic=itemView.findViewById(R.id.iv_product);
            tv_name=itemView.findViewById(R.id.tv_product_name);
            tv_price=itemView.findViewById(R.id.tv_price_product);
        }
    }
    public interface MahsulListener{
        public void mahsulCliked(Integer position);
    }

}
