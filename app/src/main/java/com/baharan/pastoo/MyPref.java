package com.baharan.pastoo;

import android.content.Context;
import android.content.SharedPreferences;

public class MyPref {

    SharedPreferences sharedPreferences;

    SharedPreferences.Editor editor;

    public MyPref(Context context){
        sharedPreferences=context.getSharedPreferences( "setting",Context.MODE_PRIVATE );
        editor=sharedPreferences.edit();
    }
    public void setIntro(boolean state){
        editor.putBoolean( "intro",state );
        editor.apply();
    }

    public boolean getIntro(){
       return sharedPreferences.getBoolean( "intro",false );
    }
}
