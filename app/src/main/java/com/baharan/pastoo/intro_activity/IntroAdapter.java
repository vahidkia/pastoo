package com.baharan.pastoo.intro_activity;


import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.baharan.pastoo.R;

import java.util.ArrayList;

public class IntroAdapter extends RecyclerView.Adapter<IntroAdapter.Holder> {

    ArrayList<Intro> intros;
    Context context;

    public IntroAdapter(ArrayList<Intro> intros , Context context) {
        this.intros=intros;
        this.context=context;
    }

    @NonNull
    @Override
    public IntroAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate( R.layout.custom_intro,parent,false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull IntroAdapter.Holder holder, int position) {

        holder.tv_onvan.setText(intros.get(position).getTitle());
        holder.iv_icon.setImageResource(intros.get(position).getIcon());
        holder.itemView.setBackgroundColor( Color.parseColor(intros.get(position).getColor()));

    }

    @Override
    public int getItemCount() {
        return intros.size();
    }
    class Holder extends RecyclerView.ViewHolder {

        TextView tv_onvan ;
        ImageView iv_icon ;


        public Holder(@NonNull View itemView) {
            super(itemView);
            tv_onvan=itemView.findViewById(R.id.tv_onvan_intro);
            iv_icon=itemView.findViewById(R.id.iv_intro);
        }
    }
}
