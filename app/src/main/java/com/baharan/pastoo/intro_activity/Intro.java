package com.baharan.pastoo.intro_activity;

public class Intro {

    String title ;
    int icon ;
    String color  ;

    public String getTitle() {
        return title;
    }

    public Intro(String title, int icon, String color) {
        this.title = title;
        this.icon = icon;
        this.color = color;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
