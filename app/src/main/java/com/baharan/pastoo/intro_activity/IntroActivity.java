package com.baharan.pastoo.intro_activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager2.widget.ViewPager2;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.baharan.pastoo.MyPref;
import com.baharan.pastoo.R;
import com.baharan.pastoo.main_activity.MainActivity;

import java.util.ArrayList;

public class IntroActivity extends AppCompatActivity {

    ArrayList<Intro> intros = new ArrayList<>();

    ViewPager2 viewPager;

    IntroAdapter introAdapter;

    Button btn_next,btn_skip;
    MyPref myPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_intro );
        viewPager=findViewById(R.id.viewpager_intro);
        myPref=new MyPref( this );

        boolean seen=myPref.getIntro();

        if (seen){
            startActivity( new Intent( IntroActivity.this, MainActivity.class ) );
            finish();
        }

        Intro into_1 = new Intro("خرید امن و مطمعن",R.drawable.ic_safe,"#F08C8C");
        Intro into_2 = new Intro("ارسال سریع و رایگان",R.drawable.ic_delivery,"#CE1FEC");;
        Intro into_3 = new Intro("تخفیفات دوره ای ویژه مشترکین",R.drawable.ic_sale,"#F3273D");
        intros.add(into_1);
        intros.add(into_2);
        intros.add(into_3);

        btn_next=findViewById( R.id.btn_next_intro );
        btn_skip=findViewById( R.id.btn_skip_intro );

        introAdapter=new IntroAdapter(intros,this);
        viewPager.setAdapter(introAdapter);

        btn_next.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                viewPager.setCurrentItem( viewPager.getCurrentItem() +1 );

            }
        } );

        btn_skip.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity( new Intent( IntroActivity.this, MainActivity.class ) );
                myPref.setIntro( true );
                finish();
            }
        } );

        viewPager.registerOnPageChangeCallback( new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected( position );

                btn_next.setTextColor( Color.parseColor( intros.get( position ).getColor() ) );
                btn_skip.setTextColor( Color.parseColor( intros.get( position ).getColor() ) );

                if (position==2){
                    btn_next.setVisibility( View.GONE );
                    btn_skip.setText( "شروع برنامه" );
                }else {
                    btn_next.setVisibility( View.VISIBLE );
                    btn_skip.setText( "رد کردن" );


                }
            }
        } );


    }
}
